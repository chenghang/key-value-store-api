import os
import sys
import logging
import configparser

def set_logging():
    rootlogger = logging.getLogger()
    rootlogger.setLevel(logging.DEBUG)
    sh = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    sh.setFormatter(formatter)
    sh.setLevel(logging.DEBUG)
    rootlogger.addHandler(sh)

def configure():
    config = configparser.ConfigParser()
    config.optionxform = str
    config_file = os.path.join(os.path.dirname(__file__), 'config.ini')

    if os.path.isfile(config_file):
        config.read(config_file)

    return config

config = configure()