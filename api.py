import json
import logging
import datetime as dt

import flask
from flask import Flask, request, abort, jsonify

from config import set_logging
set_logging()

from database.store import ValueStore

app = Flask(__name__)
applogger = logging.getLogger("APP")

def bad_request(message, code):
    response = jsonify({'message': message})
    response.status_code = code
    return response

@app.route('/object/<mykey>', methods=['GET'])
def get_from_store(mykey):

    timestamp = request.args.get('timestamp')
    if timestamp is None:
        time = dt.datetime.utcnow()
    else:
        time = dt.datetime.fromtimestamp(timestamp)

    result = ValueStore.get(mykey, time)
    if result is not None:
        value = result[0]
        return value
    else:
        applogger.debug("No result to return")
        return bad_request("No result", 404)

@app.route('/object', methods=['POST'])
def put_into_store():
    
    data = request.get_json(force=True)

    if data is None:
        applogger.debug("Unable to parse json")
        return bad_request("Unable to parse json", 400)

    try:
        for key, value in data.items():
            ValueStore.put(key, value)
    except AttributeError:
        applogger.debug("Received incorrect input type")
        return bad_request("Received incorrect input type", 400)
    
    return ("Success", 200)

@app.route('/', methods=['GET'])
def index():

    return ("Api at /object and /object/mykey", 200)

if __name__ == '__main__':
    app.run()