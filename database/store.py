import datetime as dt

from database.db import db

class ValueStore(object):

    @staticmethod
    def put(key, value):
        query = db.Query("INSERT INTO vd.key_values (key, val, last_modified) VALUES (%s, %s, %s)",
            ('key', 'val', 'last_modified'))
        args = {'key': str(key), 'val': value, 'last_modified': dt.datetime.utcnow()}
        db.run_query(query, args)

    @staticmethod
    def get(key, time=dt.datetime.utcnow()):
        query = db.Query("SELECT val FROM vd.key_values WHERE key = %s and last_modified <= %s "
                         "ORDER BY last_modified DESC LIMIT 1", ('key', 'last_modified'))
        args = {'key': str(key), 'last_modified': time}

        result = db.run_query(query, args)['tuples']
        if result:
            return result[0]
        else:
            return None