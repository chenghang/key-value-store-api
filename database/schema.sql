DO $$
BEGIN

IF NOT EXISTS(
    SELECT schema_name
        FROM information_schema.schemata
        WHERE schema_name = 'vd'
)
THEN

EXECUTE 'CREATE SCHEMA vd';

CREATE TABLE IF NOT EXISTS vd.keys (
    kid SERIAL PRIMARY KEY,
    key VARCHAR UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS vd.vals (  
    vid SERIAL PRIMARY KEY,
    kid INT REFERENCES vd.keys (kid) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
    last_modified TIMESTAMPTZ NOT NULL,
    val VARCHAR,
    UNIQUE (last_modified, val)
);

CREATE OR REPLACE VIEW vd.key_values AS 
    (SELECT key, val, last_modified
    FROM vd.keys, vd.vals
    WHERE keys.kid = vals.kid); 

CREATE OR REPLACE FUNCTION vd.insert_key_value() RETURNS trigger AS $insert$
BEGIN 
    -- we only insert if the new value is different from the last value inserted
    IF (SELECT val FROM vd.key_values WHERE key = NEW.key
        ORDER BY last_modified DESC LIMIT 1)
        IS DISTINCT FROM NEW.val THEN
    INSERT INTO vd.keys (key) VALUES (NEW.key) ON CONFLICT DO NOTHING;
    INSERT into vd.vals (kid, last_modified, val) 
        SELECT kid, NEW.last_modified, NEW.val FROM vd.keys WHERE key = NEW.key;
    END IF;

    RETURN NEW;
END; $insert$ LANGUAGE PLPGSQL;

CREATE TRIGGER key_values_insert INSTEAD OF INSERT ON vd.key_values
    FOR EACH ROW EXECUTE PROCEDURE vd.insert_key_value();



END IF;

END $$;