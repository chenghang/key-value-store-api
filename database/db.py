import sys
import os
import logging
import datetime as dt
from collections import namedtuple

import psycopg2
import psycopg2.extras
from psycopg2 import ProgrammingError, IntegrityError
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from config import config

class Dbconnector(object):
    Query = namedtuple('Query', ['sql', 'args'])

    def __init__(self):
        self.logger = logging.getLogger(__class__.__name__)
        self.cursor = self._connect()
        self.initiate()
        
    
    def _connect(self):
        self.logger.debug('Connecting to database')

        conn = psycopg2.connect(**config['postgres'])
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.logger.debug('Connected')
        
        return conn.cursor()

    def initiate(self):
        with self._connect() as curser:
            schema_path = os.path.join(os.path.dirname(__file__), 'schema.sql')
            curser.execute(open(schema_path, "r").read())

    def run_query(self, query, args):
        
        self.logger.debug('Running query {} with args {}'\
            .format(query.sql, tuple(args.keys()) ))

        if query.args:	
            args = tuple(args[arg] for arg in query.args)
        
        # run sql statement against database
        try:
            self.cursor.execute(query.sql, args)
        except IntegrityError as e:
            self.logger.debug(e)
            raise ValueError('Constraint Violated executing "{}"'.format(query))

        retval = {}
        retval['rowcount'] = self.cursor.rowcount
        self.logger.debug("Affected rowcount: {}".format(retval['rowcount']))

        try:
            retval['tuples'] = self.cursor.fetchall()
        except ProgrammingError as e:
            self.logger.debug(e)
            self.logger.debug("No result from query")
        else:
            retval['description'] = tuple(desc[0] for desc in self.cursor.description)
            self.logger.debug("Fetched columns: {} with {} rows"\
                .format(retval['description'], len(retval['tuples'])))
        
        return retval

db = Dbconnector()