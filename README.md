A version controlled key-value store with a HTTP API

GET: /object/mykey

parameters: timestamp

response: value

POST: /object

body: json: {mykey: value}